<?php include('model/class.Users.php'); 

  $users = Users::doSelect(); // do select all users
  foreach($users as $user){
    echo $user->toString(); 
  }
  
  // retrieve user by its primary key
  $getOneUser = Users::retrieveByPk(1);
  if ($getOneUser)
  {
    echo $getOneUser->toString();
  }
  
  // count all users
  $countUser = Users::doCount();
  echo "Total users: " . $countUser;
  
  // insert a new user
  $newUser = new Users();
  $newUser->setFirstname();
  $newUser->setLastname();
  $newUser->setUsername();
  $newUser->setPassword();
  $newUser->insert();
  
  // update a user
  $newUser->setFirstname("new firstname");
  $newUser->update();
  
  // delete a the inserted/updated user
  $newUser->delete();

?>