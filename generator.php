<?php 
  include_once 'class.Creator.php';
  $database = new Database();
  
  if (isset($_POST['submit']))
  {
    $table = $_REQUEST['tablename'];
    $classname = $_REQUEST['classname'];
    $keyname = $_REQUEST['keyname'];
    
    $generator = new ModelGenerator($table,$classname,$keyname);
    $generator->generate();
    $generator->controllerValidation();
    $generator->createBootstrapForm2();
    
    //header('Location: model/class.' . $classname . '.php');
  }
?>
<font face="Arial" size="3"><b>
PHP MYSQL Class Generator
</b>
</font>

<font face="Arial" size="2">
<b>

  <form action="generator.php" method="POST" name="FORMGEN">
  1) Select Table Name: 
  <br>
  <select name="tablename">
    <?php
    $database->OpenLink();
    $tablelist = mysql_list_tables($database->database, $database->link);
    while ($row = mysql_fetch_row($tablelist)) {
    print "<option value=\"$row[0]\">$row[0]</option>";
    }
    ?>
  </select>
  <p>
  2) Type Class Name (ex. "test"): <br>
  <input type="text" name="classname" size="50" value="">
  <p>
  3) Type Name of Key Field:
  <br>
  <input type="text" name="keyname" value="" size="50">
  <br>
  <font size=1>
  (Name of key-field with type number with autoincrement!)
  </font>
  <p>
  <input type="submit" name="submit" value="Generate Class">
  </form>

</b>
</font>