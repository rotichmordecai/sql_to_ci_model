<?php
/*
*
*
* This class provides one central database-connection for
* al your php applications. Define only once your connection
* settings and use it in all your applications.
*
*
*/
class Database
{ // Class : begin
 
  var $host;  		//Hostname, Server
  var $password; 	//Passwort MySQL
  var $user; 		//User MySQL
  var $database; 	//Datenbankname MySQL
  var $link;
  var $query;
  var $result;
  var $rows;
 
  function Database()
  { // Method : begin
   //Constructor
   // ********** ADJUST THESE VALUES HERE **********
 
    $this->host = "localhost";                  //          <<---------
    $this->password = "";           //          <<---------
    $this->user = "root";                   //          <<---------
    $this->database = "aj_hm";           //          <<---------
    $this->rows = 0;
 
   // **********************************************
   // **********************************************
 
 
  
  } // Method : end
 
  public function getDatabase()
  {
    return $this->database;
  }
  public function getRows()
  {
    $this->rows();
  }
 
  function OpenLink()
  { // Method : begin
    $this->link = @mysql_connect($this->host,$this->user,$this->password) or (print "Class Database: Error while connecting to DB (link)");
  } // Method : end
 
  function SelectDB()
  { // Method : begin
 
    @mysql_select_db($this->database,$this->link) or (print "Class Database: Error while selecting DB");
  
  } // Method : end
 
  function CloseDB()
  { // Method : begin
    mysql_close();
  } // Method : end
 
  function Query($query)
  { // Method : begin
    $this->OpenLink();
    $this->SelectDB();
    $this->query = $query;
    $this->result = mysql_query($query,$this->link) or (print "Class Database: Error while executing Query: " . mysql_error());
   
    // $rows=mysql_affected_rows();

    if(ereg("SELECT",$query))
    {
      $this->rows = mysql_num_rows($this->result);
    }
   
    $this->CloseDB();
  } // Method : end	
  
} // Class : end
 
?>