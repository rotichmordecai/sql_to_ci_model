<?php

/* * ********
  // Created By: Bryan Jayson Tan
  // Date Started: August 27, 2010
 */
include("model/class.database.php");

class ModelGenerator {

    private $tablename;
    private $classname;
    private $keyname;
    private $modelFolder = "model/"; // generated classes
    private $formFolder = "model/"; // generated classes
    private $controllerFolder = "model/"; // generated classes
    private $database; // database connection

    // Our Constructor

    function ModelGenerator($tablename, $classname, $keyname) {
        $this->database = new Database();

        $this->tablename = $tablename;
        $this->classname = $classname;
        $this->keyname = $keyname;
    }

    public function generate() {
        $dir = dirname(__FILE__);
        $baseFile = $dir . "/" . $this->modelFolder . "" . $this->classname . ".php";

        $this->baseFile($baseFile);
        // if file exists, then delete it


        /*
          if (!file_exists($mainFile))  // main class
          {

          } */
    }

    public function baseFile($baseFile) {
        if (file_exists($baseFile)) {
            unlink($baseFile);  // base Class
        }

        // open file in insert mode
        $file = fopen($baseFile, "w+");
        $filedate = date("d.m.Y");

        $c = "";
        $c = "<?php
/*
*
* -------------------------------------------------------
* Class name:        $this->classname
* Creation date:  $filedate
* -------------------------------------------------------
*/


// **********************
// Class declaration
// **********************

class $this->classname extends CI_Model
{

  // **********************
  // Attribute Declaration
  // **********************

  private $$this->keyname;   // Key Attribute
  ";
        $c .= $this->attributeDeclaration();
        $c .= $this->constructor();
        $c .= $this->getAndset("get");
        $c .= $this->getAndset("set");
        $c .= $this->init();
        $c .= $this->doSelect();
        $c .= $this->retrieveByPk();
        $c .= $this->delete();
        $c .= $this->insert();
        $c .= $this->update();
        $c .= $this->doCount();
        $c .= $this->fetch();
        $c .= $this->fetchCount();
        $c .= $this->search();
        $c .= $this->search_count();
        $c .= "
}
?>";
        fwrite($file, $c);
    }

    protected function showTables() {
        $sql = "SHOW COLUMNS FROM $this->tablename;";
        $this->database->query($sql);
        $result = $this->database->result;
        return $result;
    }

    protected function attributeDeclaration() {
        $databaseVar = "$" . "database";

        $result = $this->showTables();
        while ($row = mysql_fetch_row($result)) {
            $col = $row[0];
            $type = $row[1];

            if ($col != $this->keyname) {

                $c.= "
  private $$col;   // DataType: $type";
            } // endif
            //"print "$col";
        } // endwhile
        $c.= "
  
  private $databaseVar;
  ";
        return $c;
    }

    protected function constructor() {
        $databaseVar = "database";
        $cthis = "$" . "this->";
        $thisdb = $cthis . $databaseVar . " = " . "new Database();";

        $c.= "
  
  var $" . "hm;
  
  // **********************
  // Constructor Method
  // **********************

  function __construct()
  {
    parent::__construct();
    $" . "this->hm = $" . "this->load->database('hm', TRUE);
  }
  ";
        return $c;
    }

    function controllerValidation() {

        $validation = '';
        $result = $this->showTables();
        while ($row = mysql_fetch_row($result)) {
            $col = $row[0];

            $validation .= '$this->form_validation->set_rules(\'' . $col . '\', \'' . $col . '\', \'required|trim|xss_clean\');                      
';
        }
        $validation .= '
            
	
		if ($this->form_validation->run() == FALSE) 
		{
			$this->template->write_view(\'content\', $this->view_path . __FUNCTION__, $this->data);
            $this->template->render();
		}
		else 
		{
			';


        $result = $this->showTables();
        while ($row = mysql_fetch_row($result)) {
            $col = $row[0];

            $validation .= '$this->' . $this->tablename . '_model->set_' . $col . '(set_value(\'' . $col . '\'));  ';
        }

        $validation .= '					
					
			// run insert model to write data to db
		
			if ($this->' . $this->tablename . '_model->insert() > 0) 
			{
				$this->message->set(\'success\', \'Success! ' . $this->tablename . ' Added\');
			}
			else
			{
			$this->message->set(\'error\', \'Error! ' . $this->tablename . ' could not be added\');
			}
		} ';

        $dir = dirname(__FILE__);
        $baseFile = $dir . "/" . $this->controllerFolder . "function-" . $this->tablename . ".php";

        if (file_exists($baseFile)) {
            unlink($baseFile);  // base Class
        }

        // open file in insert mode
        $file = fopen($baseFile, "w+");
        $filedate = date("d.m.Y");
        fwrite($file, $validation);
    }

    function createBootstrapForm2() {


        $form = '<?php 
$attributes = array(\'class\' => \'form-horizontal\', \'id\' => \'' . $this->tablename . '-form\', \'role\' => \'form\');
echo form_open(current_url(), $attributes); 
?>

                                    <fieldset>
';


        $result = $this->showTables();
        while ($row = mysql_fetch_row($result)) {
            $col = $row[0];

            $form .= '<div class="control-group <?php echo strlen(form_error(\'' . $col . '\')) > 0 ? \'error\' : \'\' ?>">
                        <label for="' . $col . '" class="control-label">' . $col . '</label>
                        <div class="controls">
                        <input type="text" id="' . $col . '" name="' . $col . '" value="<?php echo set_value(\'' . $col . '\'); ?>"  placeholder="<?php echo set_value(\'' . $col . '\'); ?>"  class="input-xlarge focused">
                        <span class="help-inline"><?php echo form_error(\'' . $col . '\'); ?></span>
                        </div>
                      </div>
                      
';
            echo $col;
        }

        $form .= '
              <div class="form-actions">            
              <button type="submit" class="btn btn-default">Submit Button</button> 
              <button type="reset" class="btn btn-default">Reset Button</button>
              </div>
              </fieldset>
              '
                . '<?php echo form_close(); ?>';

        $dir = dirname(__FILE__);
        $baseFile = $dir . "/" . $this->formFolder . "form-" . $this->tablename . ".php";

        if (file_exists($baseFile)) {
            unlink($baseFile);  // base Class
        }

        // open file in insert mode
        $file = fopen($baseFile, "w+");
        $filedate = date("d.m.Y");
        fwrite($file, $form);
    }

    function createBootstrapForm3() {


        $form = '<?php 
$attributes = array(\'class\' => \'\', \'id\' => \'' . $this->tablename . '-form\', \'role\' => \'form\');
echo form_open(current_url(), $attributes); 
?>

';


        $result = $this->showTables();
        while ($row = mysql_fetch_row($result)) {
            $col = $row[0];

            $form .= '<div class="form-group <?php echo strlen(form_error(\'' . $col . '\')) > 0 ? \'has-error\' : \'\' ?>">
                        <label>' . $col . '</label>
                        <input class="form-control" id="' . $col . '" name="' . $col . '" value="<?php echo set_value(\'' . $col . '\'); ?>"  placeholder="<?php echo set_value(\'' . $col . '\'); ?>">
                        <p class="help-block"><?php echo form_error(\'' . $col . '\'); ?></p>
                      </div>
                      
';
            echo $col;
        }

        $form .= '<button type="submit" class="btn btn-default">Submit Button</button> 
              <button type="reset" class="btn btn-default">Reset Button</button>
              
'
                . '<?php echo form_close(); ?>';

        $dir = dirname(__FILE__);
        $baseFile = $dir . "/" . $this->formFolder . "form-" . $this->tablename . ".php";

        if (file_exists($baseFile)) {
            unlink($baseFile);  // base Class
        }

        // open file in insert mode
        $file = fopen($baseFile, "w+");
        $filedate = date("d.m.Y");
        fwrite($file, $form);
    }

    protected function getAndset($type) {
        $title = ($type == "get") ? "Getter" : "Setter";
        $type2 = ($type == "get") ? "get" : "set";
        $paren = ($type == "get") ? "()" : "($" . "value)";

        $c.="
  // **********************
  // $title Methods
  // **********************
  ";
        $result = $this->showTables();
        while ($row = mysql_fetch_row($result)) {
            $col = $row[0];
            $pieces = explode("_", $col);
            $getname = "";
            foreach ($pieces as $piece) {
                $getname .= ucfirst($piece);
            }
            $mname = $type2 . '_' . $col . $paren;
            if ($type == "get") {
                $mthis = "return $" . "this->" . $col;
            } else {
                $mthis = "$" . "this->" . $col . " = $" . "value";
            }
            $c.="
  function $mname
  {
    $mthis;
  }
  ";
        }
        return $c;
    }

    protected function init() {
        $row = "$" . "row";
        $c.="
  // **********************
  // Init Method
  // **********************

  function init($row)
  {";
        $result = $this->showTables();
        while ($row = mysql_fetch_row($result)) {
            $col = $row[0];
            $cthis = "$" . "this->" . $col . " = $" . "row->" . $col;

            $c.="
    $cthis;";
        }
        $c.="
  }
  ";
        return $c;
    }

    protected function doSelect() {
        $c.="
  // **********************
  // Select / Get all $this->tablename
  // **********************

  function select($" . "criteria = null)
  {
  
$" . "this->db->select('*');
if(is_array($" . "criteria)) {
$" . "this->db->where($" . "criteria);
}
$" . "query = $" . "this->db->get('$this->tablename');
return $" . "query->result();

}
  ";
        return $c;
    }

    protected function retrieveByPk() {
        $c.="
  // **********************
  // Get $this->tablename by $this->keyname
  // **********************

  function get_$this->tablename($" . "id)
  {
  
$" . "this->db->select('*');
$" . "this->db->where('$this->keyname', $" . "id);
$" . "query = $" . "this->db->get('$this->tablename');

foreach ($" . "query->result() as $" . "$this->tablename)
{
    $" . "this->init($" . "$this->tablename);
    return $" . "$this->tablename;
}
    
  }";
        return $c;
    }

    protected function delete() {

        $idwhere = "$" . "this->" . $this->keyname;
        $delete1 = '$this->db->where(\'' . $this->keyname . '\', ' . $idwhere . ')';
        $delete2 .= '$this->db->delete(\'' . $this->tablename . '\')';
        $delete3 .= '$this->db->affected_rows()';
        $c.="
  // **********************
  // Delete $this->tablename
  // **********************

  public function delete($" . "criteria = null)
  {
  if($" . "criteria != null) {
  $" . "this->db->delete('$this->tablename', array($" . "criteria)); 
      return $delete3;
      } else {
    $delete1;
    $delete2;
        return $delete3;
        }
    return 0;
  ";
        $c.="
  }
  ";
        return $c;
    }

    protected function insert() {
        $c.="
  // **********************
  // Insert $this->tablename
  // **********************

  function insert()
  {
                ";
        $result = $this->showTables();
        while ($row = mysql_fetch_row($result)) {
            $col = $row[0];
            if ($col != $this->keyname) {

                $cthis = 'if (isset($this->' . $col . '))';
                $cthis .= '$data[\'' . $col . '\'] = ' . "$" . "this->" . $col;
                $c.="$cthis;
                ";
            }
        }
        $c.="
       $" . "this->db->insert('$this->tablename', " . "$" . "data); 
       return $" . "this->db->insert_id();
  }
  ";
        return $c;
    }

    protected function update() {
        $c.="
  // **********************
  // Update $this->tablename
  // **********************

  function update($" . "criteria = null){
                ";
        $keyname = '';
        $result = $this->showTables();
        while ($row = mysql_fetch_row($result)) {
            $col = $row[0];
            if ($col != $this->keyname) {
                $cthis = 'if (isset($this->' . $col . '))';
                $cthis .= '$data[\'' . $col . '\'] = ' . "$" . "this->" . $col;
                $c.="$cthis;
                ";
            } else {
                $cthis = 'if (isset($this->' . $col . '))';
                $cthis .= '$data[\'' . $col . '\'] = ' . "$" . "this->" . $col;
                $c.="$cthis;
                ";
                $keyname = "$" . "this->" . $col;
            }
        }
        $c.="
            if ($keyname > 0) {
       $" . "this->db->where('$this->keyname', $keyname);
       $" . "this->db->update('$this->tablename', " . "$" . "data); 
       return $" . "this->db->affected_rows();
           } elseif($" . "criteria != null) {
  $" . "this->db->where($" . "criteria); 
      $" . "this->db->update('$this->tablename', " . "$" . "data); 
       return $" . "this->db->affected_rows();
      } else {
        return 0;
  }
  }
  ";
        return $c;
    }

    protected function search() {
        $sql = "$" . "sql";
        $criteria = "$" . "criteria";
        $classname = "$" . strtolower($this->classname);
        $result = "$" . "result";
        $thisdbquery = $classname . "->" . "database->query($" . "sql" . ")";
        $res = $classname . "->" . "database->result;";
        $row = "$" . "row";
        $rowinit = $row . '->count';


        $c.="
  // **********************
  // Search records
  // **********************

  function search($" . "search, $" . "limit, $" . "start, $" . "criteria = null, $" . "group_by = null, $" . "order_by = null)
  {
    $" . "this->db->select('*');";

        $result = $this->showTables();
        $_count = 0;
        while ($row = mysql_fetch_row($result)) {
            $col = $row[0];
            if ($col != $this->keyname) {

                if ($_count == 0) {
                    $cthis = '$this->db->like(\'' . $col . '\', $search);';
                    $c.="$cthis
                ";
                } else {
                    $cthis = '$this->db->or_like(\'' . $col . '\', $search);';
                    $c.="$cthis
                ";
                }

                $_count++;
            }
        }

        $c.="if ($" . "criteria != null && is_array($" . "criteria)) {
            if (count($" . "criteria)) {
                foreach ($" . "criteria as $" . "key => $" . "value) {
                    if (is_array($" . "value)) {
                       $" . "this->db->where_in($" . "key, $" . "value);
                    } else {
                        $" . "this->db->where($" . "criteria);
                    }
                }
            } else {
                $" . "this->db->where($" . "criteria);
            }
        }
$" . "this->db->from('$this->tablename');
    
if ($" . "group_by != NULL) {
            $" . "this->hm->group_by($" . "group_by);
        }
        
        if ($" . "order_by != NULL) {
            $" . "this->hm->order_by($" . "order_by);
        }
        
$" . "query = $" . "this->db->get();
    
if ($" . "query->num_rows() > 0) {
            return $" . "query->result();
        }
        return false;

  }";
        return $c;
    }

    protected function search_count() {
        $sql = "$" . "sql";
        $criteria = "$" . "criteria";
        $classname = "$" . strtolower($this->classname);
        $result = "$" . "result";
        $thisdbquery = $classname . "->" . "database->query($" . "sql" . ")";
        $res = $classname . "->" . "database->result;";
        $row = "$" . "row";
        $rowinit = $row . '->count';


        $c.="
  // **********************
  // Count records
  // **********************

  function search_count($" . "search, $" . "criteria = null, $" . "group_by = null, $" . "order_by = null)
  {
    $" . "this->db->select('*');";

        $result = $this->showTables();
        $_count = 0;
        while ($row = mysql_fetch_row($result)) {
            $col = $row[0];
            if ($col != $this->keyname) {

                if ($_count == 0) {
                    $cthis = '$this->db->like(\'' . $col . '\', $search);';
                    $c.="$cthis
                ";
                } else {
                    $cthis = '$this->db->or_like(\'' . $col . '\', $search);';
                    $c.="$cthis
                ";
                }

                $_count++;
            }
        }

        $c.="if ($" . "criteria != null && is_array($" . "criteria)) {
            if (count($" . "criteria)) {
                foreach ($" . "criteria as $" . "key => $" . "value) {
                    if (is_array($" . "value)) {
                       $" . "this->db->where_in($" . "key, $" . "value);
                    } else {
                        $" . "this->db->where($" . "criteria);
                    }
                }
            } else {
                $" . "this->db->where($" . "criteria);
            }
        }
$" . "this->db->from('$this->tablename');
    if ($" . "group_by != NULL) {
            $" . "this->hm->group_by($" . "group_by);
        }
        
        if ($" . "order_by != NULL) {
            $" . "this->hm->order_by($" . "order_by);
        }
$" . "query = $" . "this->db->get();
return $" . "query->num_rows();

  }";
        return $c;
    }

    protected function fetch() {
        $sql = "$" . "sql";
        $criteria = "$" . "criteria";
        $classname = "$" . strtolower($this->classname);
        $result = "$" . "result";
        $thisdbquery = $classname . "->" . "database->query($" . "sql" . ")";
        $res = $classname . "->" . "database->result;";
        $row = "$" . "row";
        $rowinit = $row . '->count';


        $c.="
  // **********************
  // Fetch records
  // **********************

  function fetch(" . "$" . "limit, $" . "start, $" . "criteria = null, $" . "group_by = null, $" . "order_by = null)
  {
    $" . "this->db->select('*');";
        $c.="if ($" . "criteria != null && is_array($" . "criteria)) {
            if (count($" . "criteria)) {
                foreach ($" . "criteria as $" . "key => $" . "value) {
                    if (is_array($" . "value)) {
                       $" . "this->db->where_in($" . "key, $" . "value);
                    } else {
                        $" . "this->db->where($" . "criteria);
                    }
                }
            } else {
                $" . "this->db->where($" . "criteria);
            }
        }
$" . "this->db->from('$this->tablename');
    if ($" . "group_by != NULL) {
            $" . "this->hm->group_by($" . "group_by);
        }
        
        if ($" . "order_by != NULL) {
            $" . "this->hm->order_by($" . "order_by);
        }
$" . "this->db->limit($" . "limit, $" . "start);
$" . "query = $" . "this->db->get();
    
if ($" . "query->num_rows() > 0) {
            return $" . "query->result();
        }
        return false;

  }";
        return $c;
    }

    protected function fetchCount() {
        $sql = "$" . "sql";
        $criteria = "$" . "criteria";
        $classname = "$" . strtolower($this->classname);
        $result = "$" . "result";
        $thisdbquery = $classname . "->" . "database->query($" . "sql" . ")";
        $res = $classname . "->" . "database->result;";
        $row = "$" . "row";
        $rowinit = $row . '->count';
        $c.="
  // **********************
  // Count records
  // **********************

  function fetch_count($" . "criteria = null, $" . "group_by = null, $" . "order_by = null)
  {
    $" . "this->db->select('*');
if ($" . "criteria != null && is_array($" . "criteria)) {
            if (count($" . "criteria)) {
                foreach ($" . "criteria as $" . "key => $" . "value) {
                    if (is_array($" . "value)) {
                       $" . "this->db->where_in($" . "key, $" . "value);
                    } else {
                        $" . "this->db->where($" . "criteria);
                    }
                }
            } else {
                $" . "this->db->where($" . "criteria);
            }
        }
$" . "this->db->from('$this->tablename');
    
if ($" . "group_by != NULL) {
            $" . "this->hm->group_by($" . "group_by);
        }
        
        if ($" . "order_by != NULL) {
            $" . "this->hm->order_by($" . "order_by);
        }
        
$" . "query = $" . "this->db->get();
return $" . "query->num_rows();

  }";
        return $c;
    }

    protected function doCount() {
        $sql = "$" . "sql";
        $criteria = "$" . "criteria";
        $classname = "$" . strtolower($this->classname);
        $result = "$" . "result";
        $thisdbquery = $classname . "->" . "database->query($" . "sql" . ")";
        $res = $classname . "->" . "database->result;";
        $row = "$" . "row";
        $rowinit = $row . '->count';
        $c.="
  // **********************
  // Count records
  // **********************

  function count()
  {
    $" . "this->db->select('*');
$" . "this->db->from('$this->tablename');
$" . "query = $" . "this->db->get();
return $" . "query->num_rows();

  }";
        return $c;
    }

}

?>